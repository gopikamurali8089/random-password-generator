const passwordBox=document.getElementById("password");
const lenthofpassword=12;
const uppercase="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const lowercase="abcdefghijklmnopqrstuvwxyz";
const numbers="0123456789";
const symbol="!@#$%^&*()_+{}[]?/><,.|:;~-_=";
const allchars=uppercase + lowercase + numbers + symbol;
let copied=document.getElementById("copied");

function passwordgen(){
    let password="";
    password+=uppercase[Math.floor(Math.random() * uppercase.length)];
    password+=lowercase[Math.floor(Math.random() * lowercase.length)];
    password+=numbers[Math.floor(Math.random() * numbers.length)];
    password+=symbol[Math.floor(Math.random() * symbol.length)];

    while(lenthofpassword > password.length)
        {
            password += allchars[Math.floor(Math.random() * allchars.length)];  
        }
   passwordBox.value = password; 
   copied.style.display="none";
}

function copypassword(){
    passwordBox.select();
    document.execCommand("copy");
    copied.style.display="block";
}


